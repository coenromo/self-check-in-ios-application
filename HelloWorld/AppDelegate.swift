//
//  AppDelegate.swift
//  HelloWorld
//
//  Created by Coen on 26/6/20.
//  Copyright © 2020 Coen. All rights reserved.
//

import UIKit

import UserNotifications

import EstimoteProximitySDK

import SwiftUI

import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?

    var proximityObserver: ProximityObserver!
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        CLLocationManager.authorizationStatus()
        
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            print("notifications permission granted = \(granted), error = \(error?.localizedDescription ?? "(none)")")
        }

        let estimoteCloudCredentials = CloudCredentials(appID: "ios-app-notification-credentials", appToken: "APP_TOKEN")

        proximityObserver = ProximityObserver(credentials: estimoteCloudCredentials, onError: { error in
            print("ProximityObserver error: \(error)")
        })

        let zone = ProximityZone(tag: "ios-app-notification-credentials", range: ProximityRange.near)
        zone.onEnter = { context in
            let content = UNMutableNotificationContent()
            content.title = "Welcome to Centrelink Fortitude Valley!"
            content.body = "Verify your details to perform a self check-in and stay COVIDSafe."
            content.sound = UNNotificationSound.default
            content.launchImageName = "Notification"
            let request = UNNotificationRequest(identifier: "enter", content: content, trigger: nil)
            notificationCenter.add(request, withCompletionHandler: nil)
        }
        zone.onExit = { context in
            let content = UNMutableNotificationContent()
            content.title = "Thanks for stopping by!"
            content.body = "Check out our Express Plus mobile apps for other ways you can perform self service."
            content.sound = UNNotificationSound.default
            let request = UNNotificationRequest(identifier: "exit", content: content, trigger: nil)
            notificationCenter.add(request, withCompletionHandler: nil)
        }

        proximityObserver.startObserving([zone])
        
        // Override point for customization after application launch.
        return true
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate: UNUserNotificationCenterDelegate {

    // Needs to be implemented to receive notifications both in foreground and background
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([UNNotificationPresentationOptions.alert, UNNotificationPresentationOptions.sound])
    }
    
    //test notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let navigationController =  window?.rootViewController as? UINavigationController {
            navigationController.popToRootViewController(animated: false)
            if let someViewController = navigationController.storyboard?.instantiateViewController(withIdentifier: "NotificationScreen") as? NotificationScreen {
                navigationController.pushViewController(someViewController, animated: true)
            }
        }
        completionHandler()
    }
}
