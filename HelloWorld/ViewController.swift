//
//  ViewController.swift
//  HelloWorld
//
//  Created by Coen on 26/6/20.
//  Copyright © 2020 Coen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //declare name and CRN var
    var fullName: String?
    var crn: String?
    
    /* TEXT-FIELD CRN */
    @IBOutlet weak var textCRN: UITextField!
    
    //text did change
    @IBAction func changedCRNText(_ sender: Any) {
        if let value = textCRN.text {
            print(value, "text value")
        }
    }
    
    // return pressed on keyboard
    @IBAction func end(_ sender: UITextField) {
        print("end function   CRN ...")
        sender.resignFirstResponder()
    }
    
    /* TEXT-FIELD NAME */
    @IBOutlet weak var textName: UITextField!
    
    //text did change
    @IBAction func changedNameText(_ sender: Any) {
        if let value = textName.text {
            print(value, "text value")
        }
    }
    
    // return pressed on keyboard
    @IBAction func endName(_ sender: UITextField) {
        print("end function   ...")
        //textFieldShouldReturn(sender)
        sender.resignFirstResponder()
    }
    
    @IBAction func showMessage(sender: UIButton) {
        let alertController = UIAlertController(title: "Force check-in?", message: "Only use if unable to connect to Services Australia Wi-Fi", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: showHomeScreen))
        present(alertController, animated: true, completion: nil)
    }
    
    //showHomeScreen to force if cannot connect to wifi to perform POST
    func showHomeScreen(alertAction: UIAlertAction) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(identifier: "ThirdViewController")
        homeScreen.modalPresentationStyle = .fullScreen
        homeScreen.modalTransitionStyle = .coverVertical

        show(homeScreen, sender: self)
    }
    
    //prepare data for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is SecondViewController {
            let vc = segue.destination as? SecondViewController
            // set data
            vc?.crn = textCRN.text ?? "NOCRN"
            vc?.fullName = textName.text ?? "NONAME"
        }
    }
    
    //perform segue to secondViewController
    @IBAction func onGoButton(_ sender: Any) {
        performSegue(withIdentifier: "mySegue", sender: self)
        //store name and CRN values
        //self.fullName = textName.text ?? "<name_not_specified>"
        //self.crn = textCRN.text ?? "<CRN_not_specified>"
        //print("name: ", self.fullName ?? "No Name specified", " CRN: ", self.crn ?? "No CRN specified")
    }
    
    @IBAction func onGoNotify(_ sender: Any) {
        performSegue(withIdentifier: "notifySegue", sender: self)
    }
    //segue to notification view
}

