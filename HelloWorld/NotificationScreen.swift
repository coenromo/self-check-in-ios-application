import UIKit
import UserNotifications
import MapKit
import CoreLocation
import SwiftUI

class NotificationScreen: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    private let locationNotificationScheduler = LocationNotificationScheduler()
    
    //notification content
    var notifyBody: String = ""
    
    //not linked yet
    @IBOutlet weak var backButton: UIButton!
    
    //region2
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    //mapview
    let locationManager = CLLocationManager()

    //notifications
    let notificationCenter = UNUserNotificationCenter.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationNotificationScheduler.delegate = self
        
        //map view, locationManager
        locationManager.delegate = self
        locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        //setup mapview
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        
        //test data
        setupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let size = CGSize(width: 20, height: 30)
        
        backButton.layer.cornerRadius = 15
        backButton.titleLabel?.shadowOffset = size
        
        // 1. status is not determined
        if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestAlwaysAuthorization()
            }
            // 2. authorization were denied
        else if CLLocationManager.authorizationStatus() == .denied {
                print("Location services were previously denied. Please enable location services for this app in Settings.")
            }
            // 3. we do have authorization
        else if CLLocationManager.authorizationStatus() == .authorizedAlways {
                locationManager.startUpdatingLocation()
            }
    }
    
    @IBOutlet weak var colourButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    /// trigger alert popup to go to segue i.e. home
    func showMessage() {
        let alertController = UIAlertController(title: "Goodbye!", message: "Thanks for staying safe and performing a self check-in.", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: showCheckInScreen))
        present(alertController, animated: true, completion: nil)
    }
    
    //force show check in screen
    func showCheckInScreen(alertAction: UIAlertAction) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(identifier: "Home")
        homeScreen.modalPresentationStyle = .fullScreen
        homeScreen.modalTransitionStyle = .coverVertical

        show(homeScreen, sender: self)
    }
    
    /*
    * alternative to using estimote beacons
    * ios allows you to setup GEO location fences
    * rather than triggering on proximity to the beacon they track GPS regions and alert
    * the user when the device comes into contact with the CLCircularRegion
    */
    func setupData() {
        // 1. check if system can monitor regions
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
     
            // 2. region data
            let title = "Centrelink Fortitude Valley"
            let coordinate = CLLocationCoordinate2DMake(-27.4662587, 153.0265357)
            let regionRadius = 50.0
     
            // 3. setup region
            let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,
                longitude: coordinate.longitude), radius: regionRadius, identifier: title)
            locationManager.startMonitoring(for: region)
     
            // 4. setup annotation
            let restaurantAnnotation = MKPointAnnotation()
            restaurantAnnotation.coordinate = coordinate;
            restaurantAnnotation.title = "\(title)";
            mapView.addAnnotation(restaurantAnnotation)
     
            // 5. setup circle
            let circle = MKCircle(center: coordinate, radius: regionRadius)
            mapView.addOverlay(circle)
        }
        else {
            print("System can't track regions")
        }
    }
     
    // 6. draw circle
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.strokeColor = UIColor.red
        circleRenderer.lineWidth = 1.0
        return circleRenderer
    }
    
    //map regions
    // 1. user enter region
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("enter " + region.identifier)
        colourButton.backgroundColor = .green
        colourButton.setTitle("Enter Region Range", for: .normal)
        
        //try to notify
        scheduleNotification(notificationType: "Welcome to Centrelink Fortitude Valley!")
        self.notifyBody = "Follow the steps to perform a self check-in? (CLCircularRegion)"
    }
     
    // 2. user exit region
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("exit \(region.identifier)")
        colourButton.backgroundColor = .red
        colourButton.setTitle("Exit Region Range", for: .normal)

        //show alert - for debugging
        //showMessage()
        
        //notify exit
        scheduleNotification(notificationType: "You have exited the region")
        self.notifyBody = "Thanks for performing a self check in. (CLCircularRegion)"
    }
    
    func scheduleNotification(notificationType: String) {
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
     
            // 2. region data
            let title = "Centrelink"
            let coordinate = CLLocationCoordinate2DMake(-27.4662587, 153.0265357)
            let regionRadius = 50.0
     
            // 3. setup region
            let region2 = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,
                longitude: coordinate.longitude), radius: regionRadius, identifier: title)
            locationManager.startMonitoring(for: region2)
     
            // 4. setup annotation
            let restaurantAnnotation = MKPointAnnotation()
            restaurantAnnotation.coordinate = coordinate;
            restaurantAnnotation.title = "\(title)";
            mapView.addAnnotation(restaurantAnnotation)
     
            // 5. setup circle
            let circle = MKCircle(center: coordinate, radius: regionRadius)
            mapView.addOverlay(circle)
        
        
        let content = UNMutableNotificationContent()
        let userActions = "User Actions"
        content.title = notificationType
        content.body = self.notifyBody
        content.sound = UNNotificationSound.defaultCritical
        content.badge = 1
        content.categoryIdentifier = userActions
        
        //trigger
        let trigger = UNLocationNotificationTrigger(region: region2, repeats: true)
        
        //schedule
        let request = UNNotificationRequest(identifier: "test", content: content, trigger: trigger)
        notificationCenter.add(request, withCompletionHandler: nil)

            let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
                let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
                let category = UNNotificationCategory(identifier: userActions, actions: [snoozeAction, deleteAction], intentIdentifiers: [], options: [])
                
                notificationCenter.setNotificationCategories([category])
        }
    }
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension NotificationScreen: LocationNotificationSchedulerDelegate {
    
    func locationPermissionDenied() {
        let message = "The location permission was not authorized. Please enable it in Settings to continue."
        presentSettingsAlert(message: message)
    }
    
    func notificationPermissionDenied() {
        let message = "The notification permission was not authorized. Please enable it in Settings to continue."
        presentSettingsAlert(message: message)
    }
    
    func notificationScheduled(error: Error?) {
        if let error = error {
            let alertController = UIAlertController(title: "Notification Schedule Error",
                                                    message: error.localizedDescription,
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true)
        } else {
            let alertController = UIAlertController(title: "Notification Scheduled!",
                                                    message: "You will be notified when you are near the location!",
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true)
        }
    }
    
    //didrecieve is triggering, change to category and open specific screen?
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.identifier == "test" {
            let notificationData = response.notification.request.content.userInfo
            let message = "You have reached \(notificationData["location"] ?? "your location!")"
            
            let alertController = UIAlertController(title: "Welcome!",
                                                    message: message,
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true)
        }
        completionHandler()
    }
    
    private func presentSettingsAlert(message: String) {
        let alertController = UIAlertController(title: "Permissions Denied!",
                                                message: message,
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            if let appSettings = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettings)
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        present(alertController, animated: true)
    }
}
