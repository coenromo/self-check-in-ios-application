//
//  SecondViewController.swift
//  HelloWorld
//
//  Created by Coen on 29/6/20.
//  Copyright © 2020 Coen. All rights reserved.
//

import UIKit
import Foundation

class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    //pull data from first view
    var fullName: String = ""
    var crn: String = ""
    var details: String = ""
    
    //segmentcontrol
    var segmentSelected: String = ""
    
    //hide uipicker in text field
    @IBOutlet weak var UIPickerHide: UITextField!
    
    //assign to label
    @IBOutlet weak var fullNameLabel:UILabel?
    @IBOutlet weak var crnLabel:UILabel?
    
    //map fields
    //UIPicker
    @IBOutlet weak var picker: UIPickerView!
    
    //ui segmentedControl
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textLabel: UILabel?
    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
            {
            case 0:
                print(textLabel?.text ?? "ATO")
                //already know ID of selected text, but the OData POST only accepts the key
                self.segmentSelected = textLabel?.text ?? "1026"
                print("segmentSELECTED", self.segmentSelected)
            case 1:
                print(textLabel?.text ?? "Medicare")
                self.segmentSelected = textLabel?.text ?? "1030"
                print("segmentSELECTED", self.segmentSelected)
            case 2:
                print(textLabel?.text ?? "Child Support")
                self.segmentSelected = textLabel?.text ?? "1029"
                print("segmentSELECTED", self.segmentSelected)
            default:
                print(textLabel?.text ?? "ATO")
                self.segmentSelected = textLabel?.text ?? "1026"
                print("DEFAULT", self.segmentSelected)
                break
            }
    }
    
    
    
    var pickerData:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //retrieve data
        fullNameLabel?.text = fullName
        crnLabel?.text = crn
        print("fullName: ", fullName, "CRN: ", crn)

        // Do any additional setup after loading the view.
        
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        
        // input data into the array
        pickerData = ["Lodge a Document", "Booked Appointments", "Claim", "Concession Cards", "Enquiry"]
        
        //select default picker row
        pickerView(picker, didSelectRow: 0, inComponent: 0)
        
        //segmentedControl default
        indexChanged(self)
    }
    
    //post bool var
    var success: Bool = false
    
    //declare var
    var postID: Int = 0
    var stringID = ""
    
    //pickerview variables
    var test = 0
    var walkInItem = ""
    
    /* TEXT-FIELD Details */
    @IBOutlet weak var textDetails: UITextField!
    
    //text did change
    @IBAction func changedDetailText(_ sender: Any) {
        if let value = textDetails.text {
            self.details = textDetails.text ?? "No Details"
            print(self.details, "text value", value)
        }
    }
    
    // return pressed on keyboard
    @IBAction func end(_ sender: UITextField) {
        print("end function   Details ...")
        sender.resignFirstResponder()
    }
    
    //keyboard
    @IBAction func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @IBAction func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        print(row, component, "                 PICKER selection")
        
        //get picker data selection
        test = row
        print("Picker Text:   ", self.pickerData[test])
        
        //do if for picker view data, pulled from FOH.HISTORY.QUEUE in T3
        // pickerData = ["Lodge a Document", "Booked Appointments", "Claim", "Concession Cards", "Enquiry"]
        switch test {
        case 0:
            walkInItem = "1027"
        case 1:
            walkInItem = "1022"
        case 2:
            walkInItem = "1017"
        case 3:
            walkInItem = "1019"
        case 4:
            walkInItem = "1025"
        default:
            walkInItem = "1027"
        }
    }
    
    // confirm alert after cusomer succesfully books in
    @IBAction func showMessage(sender: UIButton) {
        //run HTTP PUT requests
        self.nsURL()
        
        //delay bc need sychronous programming
        usleep(900000) //sleep for 0.9 seconds, hacky fix, need more time to implement synchronous 
        
        //check if PUT succeeded
        if (self.success == true) {
            print("if else, success hit, HIT")
               let alertController = UIAlertController(title: "Success!", message: "Self check in confirmed.", preferredStyle: UIAlertController.Style.alert)
               alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: showSecondViewController))
            present(alertController, animated: true, completion: nil)
        } else {
            print("if else, Fail hit, HIT")
            let alertController = UIAlertController(title: "Failed to perform self check in at this time.", message: "Please ensure you are in range of the Service Centre and connected to Services Australia Wi-Fi.", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: goHome))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    //working POST request of CRUD
    // could move this into repeatable function in the future but this is a MVP
    func nsURL() {
        let login = "USER_NAME"
        let password = "USER_PASSWORD"
        
        let urlMe = URL(string: "SAP-ODATA-URL/walkinCRUD")
        var request = URLRequest(url: urlMe!)
        
        //add request POST values
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("multipart/mixed", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(login):\(password)"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8, allowLossyConversion: true)
        let base64EncodedCredential = userPasswordData!.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization": authString]
        let session = URLSession(configuration: config)
        
            //convert data to json
        let jsonDictionary: [String: String] = [
            "LOCATION_ID":"61012608",
            "STATUS":"NEW",
            "ID":"0"
        ]

        let dataBody = try! JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        print(dataBody, "JSON DATA")
        
        
        //send request
        session.uploadTask(with: request, from: dataBody) { (responseData, response, error) in
            if let error = error {
                print("Error making PUT request - CRUD: \(error.localizedDescription)", error)
                print("success is ", self.success)
                self.success = false
                print("success is ", self.success)
                return
            }
            
            if let responseCode = (response as? HTTPURLResponse)?.statusCode, let responseData = responseData {
                guard responseCode == 201 else {
                    print("Invalid response code - CRUD: \(responseCode)", responseData)
                    print("success is ", self.success)
                    self.success = false
                    print("success is ", self.success)
                    return
                }
                
                if let responseJSONData = try? JSONSerialization.jsonObject(with: responseData, options: []) {
                    print("Response JSON data - CRUD = \(responseJSONData)")
                    
                    //retreive ID from CRUD JSON response
                    if let dictionary = responseJSONData as? [String: Any] {
                        if let number = dictionary["CHANGE_USER"] as? Double {
                            // access individual value in dictionary
                            print(number, "number")
                        }

                        // access ID value
                        if let nestedDictionary = dictionary["d"] as? [String: Any] {
                            // access nested dictionary values by key
                            print("ID: ", nestedDictionary["ID"]!)
                            self.postID = nestedDictionary["ID"]! as! Int
                            self.stringID = String(self.postID) //convert to string
                        }
                    }
                    print("success is ", self.success)
                    self.success = true
                    print("success is ", self.success)
                    self.nsURLPut()
                }
            }
        }.resume()
    }
    
    // POST request, PUT
    func nsURLPut() {
            print("nsURLPut", "......PUT Called")
            let login = "USER_NAME"
            let password = "USER_PASSWORD"
            
            // need to dynamically retrieve the ID from the previous OData GET
        let urlMe = URL(string: "SAP-URL-ODATA/walkinCRUD(" + self.stringID + ")")
            var request = URLRequest(url: urlMe!)
            
            //add request POST values
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("multipart/mixed", forHTTPHeaderField: "Content-Type")
            
            let config = URLSessionConfiguration.default
            let userPasswordString = "\(login):\(password)"
            let userPasswordData = userPasswordString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64EncodedCredential = userPasswordData!.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
            let authString = "Basic \(base64EncodedCredential)"
            config.httpAdditionalHeaders = ["Authorization": authString]
            let session = URLSession(configuration: config)
            
                //convert data to json
            let jsonDictionary: [String: String] = [
                "ID": self.stringID,
                    "LOCATION_ID": "61012608",
                "CUSTOMER_ID": self.crn,
                    "PARTNER_ID": "0110605075",
                "CUSTOMER_NAME": self.fullName,
                    "STATUS": "WAITING",
                "NOTES": self.details,
                    "ASSIGNED_USER": "",
                    "CHANGE_REASON": "walkin.xsjslib::createWalkin",
                    "CHANGE_USER": "MBS372",
                    "CHANGE_DATE": "/Date(1593566231999)/",
                    "WALKIN_ETAG": "2020-07-01T01:17:11.571Z",
                    "CREATE_DATE": "2020-07-01T01:17:11.573Z"
            ]

            let dataBody = try! JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
            print(dataBody, "JSON DATA")
            
            //send request
            session.uploadTask(with: request, from: dataBody) { (responseData, response, error) in
                if let error = error {
                    print("Error making PUT request - PUT: \(error.localizedDescription)", error)
                    print("success is ", self.success)
                    self.success = false
                    print("success is ", self.success)
                    return
                }
                
                if let responseCode = (response as? HTTPURLResponse)?.statusCode, let responseData = responseData {
                    guard responseCode == 204 else {
                        print("Invalid response code - PUT: \(responseCode)", responseData)
                        print("success is ", self.success)
                        self.success = false
                        print("success is ", self.success)
                        return
                    }
                    
                    if let responseJSONData = try? JSONSerialization.jsonObject(with: responseData, options: .allowFragments) {
                        print("Response JSON data - PUT = \(responseJSONData)")
                    }
                    print("success is ", self.success)
                    self.success = true
                    print("success is ", self.success)
                    self.nsURLItem()
                }
            }.resume()
        }
    
    //POST request ITEM
    func nsURLItem() {
            let login = "USER_NAME"
            let password = "USER_PASSWORD"
            
            let urlMe = URL(string: "SAP-URL/walkinItem")
            var request = URLRequest(url: urlMe!)
            
            //add request POST values
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("multipart/mixed", forHTTPHeaderField: "Content-Type")
            
            let config = URLSessionConfiguration.default
            let userPasswordString = "\(login):\(password)"
            let userPasswordData = userPasswordString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64EncodedCredential = userPasswordData!.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
            let authString = "Basic \(base64EncodedCredential)"
            config.httpAdditionalHeaders = ["Authorization": authString]
            let session = URLSession(configuration: config)
            
                //convert data to json
            let jsonDictionary: [String: String] = [
                "WALKIN_ID": self.stringID,
                "QUEUE_ID": self.segmentSelected,
                    "CONTACT_TYPE_ID": self.walkInItem,
                    "CONTACT_REASON_ID": "0",
                    "ID": "0"
            ]

            let dataBody = try! JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
            print(dataBody, "JSON DATA")
            
            //send request
            session.uploadTask(with: request, from: dataBody) { (responseData, response, error) in
                if let error = error {
                    print("Error making PUT request - ITEM: \(error.localizedDescription)", error)
                    print("success is ", self.success)
                    self.success = false
                    print("success is ", self.success)
                    return
                }
                
                if let responseCode = (response as? HTTPURLResponse)?.statusCode, let responseData = responseData {
                    guard responseCode == 201 else {
                        print("Invalid response code - ITEM: \(responseCode)", responseData)
                        print("success is ", self.success)
                        self.success = false
                        print("success is ", self.success)
                        return
                    }
                    
                    if let responseJSONData = try? JSONSerialization.jsonObject(with: responseData, options: .allowFragments) {
                        print("Response JSON data - ITEM = \(responseJSONData)")
                        print("success is ", self.success)
                        self.success = true
                        print("success is ", self.success)
                    }
                }
            }.resume()
        }
    
    //go to secondviewcontroller
    func showSecondViewController(alertAction: UIAlertAction) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(identifier: "ThirdViewController")
        secondVC.modalPresentationStyle = .fullScreen
        secondVC.modalTransitionStyle = .flipHorizontal

        show(secondVC, sender: self)
    }
    
    //go home
    func goHome(alertAction: UIAlertAction) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let home = storyboard.instantiateViewController(identifier: "Home")
        home.modalPresentationStyle = .fullScreen
        home.modalTransitionStyle = .coverVertical

        show(home, sender: self)
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
