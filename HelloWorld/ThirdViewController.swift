//
//  ThirdViewController.swift
//  HelloWorld
//
//  Created by Coen on 30/6/20.
//  Copyright © 2020 Coen. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation
import SwiftUI

class ThirdViewController: UIViewController {

    let notificationCenter = UNUserNotificationCenter.current()
    
    /**
    * PUBLIC VARS
    */
    
    //OData VAR
    var httpGet: Bool = false
    var loc: String = ""
    
    //retreiving json values
    var postID: Int = 0
    var stringID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getWalkIn()
        // Do any additional setup after loading the view.
    }
    
    ///createdDate pulled from json response
    @IBOutlet weak var dateCreated: UILabel!
    
    //settings but not really setings
    @IBAction func showMessage(sender: UIButton) {
        let alertController = UIAlertController(title: "Force Check-In?", message: "i.e. unable to trigger estimote", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: showCheckInScreen))
        present(alertController, animated: true, completion: nil)
    }
    
    //force show check in screen
    func showCheckInScreen(alertAction: UIAlertAction) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(identifier: "Home")
        homeScreen.modalPresentationStyle = .fullScreen
        homeScreen.modalTransitionStyle = .coverVertical

        show(homeScreen, sender: self)
    }
    
    //segue to notification view
    @IBAction func onGo(_ sender: Any) {
        performSegue(withIdentifier: "notifySegue", sender: self)
    }
    
    //trigger get to find walk in status
    func getWalkIn() {
        let login = "USER_NAME"
        let password = "USER_PASSWORD"
        
        let urlMe = URL(string: "SAP-URL/walkinCRUD(1004140)")
        var request = URLRequest(url: urlMe!)
        
        //add request POST values
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("multipart/mixed", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(login):\(password)"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8, allowLossyConversion: true)
        let base64EncodedCredential = userPasswordData!.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization": authString]
        let session = URLSession(configuration: config)
        
            //convert data to json
        let jsonDictionary: [String: String] = [:]

        let dataBody = try! JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        print(dataBody, "JSON DATA")
        
        //send request
        session.dataTask(with: request) { (responseData, response, error) in
            if let error = error {
                print("Error making PUT request - ITEM: \(error.localizedDescription)", error)
                print("success is ", self.httpGet)
                self.httpGet = false
                print("success is ", self.httpGet)
                return
            }
            
            //change response code 200/201 etc to the response you expect to see so it hits the next if let
            if let responseCode = (response as? HTTPURLResponse)?.statusCode, let responseData = responseData {
                guard responseCode == 200 else {
                    print("Invalid response code - ITEM: \(responseCode)", responseData)
                    print("success is ", self.httpGet)
                    self.httpGet = false
                    print("success is ", self.httpGet)
                    return
                }
                
                if let responseJSONData = try? JSONSerialization.jsonObject(with: responseData, options: .allowFragments) {
                    print("Response JSON data - ITEM = \(responseJSONData)")
                    
                    ///retreive data from CRUD JSON response
                    if let dictionary = responseJSONData as? [String: Any] {
                        if let number = dictionary["CHANGE_USER"] as? Double {
                            // access individual value in dictionary
                            print(number, "number")
                        }

                        // access ID value
                        if let nestedDictionary = dictionary["d"] as? [String: Any] {
                            // access nested dictionary values by key
                            print("CREATE_DATE: ", nestedDictionary["CREATE_DATE"]!)
                            self.stringID = nestedDictionary["CREATE_DATE"]! as! String
                            self.dateFormat()
                        }
                    }
                    
                    print("success is ", self.httpGet)
                    self.httpGet = true
                    print("success is ", self.httpGet)
                    // look into queue status notifications based on position of # in OData
                    self.scheduleNotification(notificationType: "Alert: You are third in the queue!")
                    
                    
                   
                }
            }
        }.resume()
    }
    
    ///format date
    func dateFormat () {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSSS"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE, MMM d, yyyy"
        
        if let date = dateFormatterGet.date(from: self.stringID) {
            print(dateFormatterPrint.string(from: date))
            
            //do UI stuff in main thread
            DispatchQueue.main.async {
                //try to assign to label
                self.dateCreated.text = dateFormatterPrint.string(from: date)
                print("dateLabel = ", self.dateCreated.text!)
            }
        } else {
            print("error in date decoding")
        }
    }
    
    //notification to check status in queue
    func scheduleNotification(notificationType: String) {
        
        if (httpGet == true) {
            print("http = ", httpGet)
            //notification content
            let content = UNMutableNotificationContent()
            let userActions = "User Actions"
            content.title = notificationType
            content.body = "Estimated wait time is 15 minutes."
            content.sound = UNNotificationSound.defaultCritical
            content.badge = 1
            content.categoryIdentifier = userActions
            
            //trigger
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
            
            //schedule
            let request = UNNotificationRequest(identifier: "test", content: content, trigger: trigger)
            
            //add to notification centre
            notificationCenter.add(request, withCompletionHandler: nil)

            //snooze actions, only needed if categoryIdentifiers exist
                let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
                    let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
                    let category = UNNotificationCategory(identifier: userActions, actions: [snoozeAction, deleteAction], intentIdentifiers: [], options: [])
                    
                    notificationCenter.setNotificationCategories([category])
        }
        print("http = ", httpGet)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
