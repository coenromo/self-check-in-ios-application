# Self Check In iOS Application

Mobile iOS application that uses OData to retrieve JSON values and POST check-in data to Centrelink to perform a self check-in for customers when they have come into contact with either and Estimote beacon or a CLCircularRegion

## The Issue 

Currently customers walk into a service centre and are greeted by a service centre representative, who asks them their reason for visiting centrelink and adds them to the waitroom to speak to a staff member after they have confirmed their details. Although this process works, it could be improved upon for the younger generation.

## Improvement

There are many 'ordering' applications and systems that allow the customers to perform their duties autonoumosly. There is no requirement in the government to sight the customer when performing a check-in, especially when you consider that we transitioned to this model from previously allowing the customer to perform an assisted check-in with the mobile tablets positioned at the entrance.

Customers who have the Centrelink Express Mobile Application should be able to perform a self check-in when in range of a service centre, and also be updated via notification on their place in the queue. 

## Why

1. Free up Service Centre staff who would normally be providing this check-in service to assist customers with their needs 
2. Allow tech-savvy and time poor customers to perform a self check-in when in range of a service centre, while also giving them real time updates on their place in the queue so they can perform other tasks while on the waitlist
3. Allow Service Centre staff to spend more time with customers who require extra assistance 
4. Continue to innovate and show Australia that the government is responding to real world problems with new age solutions 

## The Solution

* This application uses Estimote beacons to trigger a notification on the Centrelink App to alert the customer that they are able to perform a self check-in
    * This was also developed with an iOS only solution of using CLCircularRegions rather than Estimote beacons. This would save time and money in installing the beacons in all Service Centre locations, however the Estimote SDK has been built and tested for these exact purposes, so more testing may be rquired
* Once at the check-in screen, we do an initial GET to the database to retrieve the customers place in the queue. Then using that GET we allow the customer to write to the back-end database table with their details and customer reference number (CRN) by doing a POST
* After check-in, we alert the customer of their place in the queue 

